import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View } from "react-native";
import QuizScreen from "./src/screens/QuizScreen";

export default function App() {
  return <QuizScreen />;
}
