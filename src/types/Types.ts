type QuestionDataType = {
  id: string;
  type: string;
  question: string;
  number?: number | number[];
  options?: number[];
  correct: boolean;
};

// Should be of type Array of objects
type ResultsDataType = {
  correct: boolean;
  questionType: string;
  userInput: number;
};
export { QuestionDataType, ResultsDataType };
