import {
  NUMBER_MULTIPLE_CHOICE,
  FILL_IN_THE_BLANK_SINGLE,
  FILL_IN_THE_BLANK_MULTI,
  TEN_FRAME,
} from "../../constants/Variables";
import {
  oneToTenNumber,
  checkMultiple,
  multiplesOfFiveArr,
} from "../../utils/common";

export default [
  {
    id: "q1",
    type: TEN_FRAME,
    question: "Enter the total amount",
    number: oneToTenNumber(),
  },
  {
    id: "q2",
    type: NUMBER_MULTIPLE_CHOICE,
    question: "Select the multiple of 5",
    options: multiplesOfFiveArr,
    correct: checkMultiple(multiplesOfFiveArr, 5),
  },
  {
    id: "q3",
    type: TEN_FRAME,
    question: "Enter the total amount",
    number: oneToTenNumber(),
  },
  {
    id: "q4",
    type: FILL_IN_THE_BLANK_SINGLE,
    options: [3, 9, 8, 12],
    correct: 12,
    text: "0,  3,  6,  9,",
    question: "Select the next number in the sequence",
  },
  {
    id: "q5",
    type: FILL_IN_THE_BLANK_MULTI,
    options: [7, 1, 8, 2],
    question: "Select the 2 numbers that match the sequence",
    parts: [
      {
        number: 1,
        isBlank: true,
      },
      {
        number: 4,
      },
      {
        number: 8,
        isBlank: true,
      },
      {
        number: 13,
      },
    ],
  },
];
