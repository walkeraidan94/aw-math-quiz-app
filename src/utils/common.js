export const oneToTenNumber = () => Math.floor(Math.random() * 10);
export const checkMultiple = (numArr, multiple) =>
  numArr.filter((x) => x % multiple === 0);
export const multiplesOfFiveArr = [15, 2, 9, 4];
