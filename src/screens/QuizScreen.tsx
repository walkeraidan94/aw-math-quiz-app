import React, { useState, useEffect } from "react";
import Header from "../components/Header";
import GridSelectorOption from "../components/GridSelectorOption";
import questionsData from "../assets/data/questionsData";

import { ScreenContainer } from "../components/styles/layouts/Containers";
import {
  FILL_IN_THE_BLANK_SINGLE,
  FILL_IN_THE_BLANK_MULTI,
  NUMBER_MULTIPLE_CHOICE,
  TEN_FRAME,
} from "../constants/Variables";
import FillInTheBlankSingle from "../components/FillInTheBlankSingle";
import FillInTheBlankMulti from "../components/FillInTheBlankMulti";
import TenFrame from "../components/TenFrame";
import Results from "../components/Results/Results";
import { QuestionDataType, ResultsDataType } from "../types/Types";

const QuizScreen = () => {
  // Sets question
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  const [currentQuestion, setCurrentQuestion] = useState<QuestionDataType | {}>(
    questionsData[currentQuestionIndex]
  );
  // Stores question answers
  const [results, setResults] = useState<ResultsDataType | []>([]);

  console.log(results);
  // Determines which question to load
  useEffect(() => {
    if (currentQuestionIndex === questionsData.length) {
      setCurrentQuestion(0);
    } else {
      setCurrentQuestion(questionsData[currentQuestionIndex]);
    }
  }, [currentQuestionIndex]);

  const setNextQuestion = () =>
    setCurrentQuestionIndex(currentQuestionIndex + 1);

  // Determines progress bar style width
  const progress = currentQuestionIndex / questionsData.length;

  return (
    <ScreenContainer>
      {/* Results component */}
      {progress === 1 && <Results results={results} />}
      {/* Progress bar component */}
      {progress !== 1 && <Header progress={progress} />}
      <>
        {currentQuestion?.type === TEN_FRAME && (
          <TenFrame
            question={currentQuestion}
            setNextQuestion={setNextQuestion}
            results={results}
            setResults={setResults}
          />
        )}
        {currentQuestion.type === NUMBER_MULTIPLE_CHOICE && (
          <GridSelectorOption
            question={currentQuestion}
            setNextQuestion={setNextQuestion}
            results={results}
            setResults={setResults}
          />
        )}
        {currentQuestion.type === FILL_IN_THE_BLANK_SINGLE && (
          <FillInTheBlankSingle
            question={currentQuestion}
            setNextQuestion={setNextQuestion}
            results={results}
            setResults={setResults}
          />
        )}
        {currentQuestion.type === FILL_IN_THE_BLANK_MULTI && (
          <FillInTheBlankMulti
            question={currentQuestion}
            setNextQuestion={setNextQuestion}
            results={results}
            setResults={setResults}
          />
        )}
      </>
    </ScreenContainer>
  );
};

export default QuizScreen;
