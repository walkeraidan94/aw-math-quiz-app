import React, { useState } from "react";
import styled from "styled-components/native";
import Colors from "../../constants/Colors";
import AnswerIcon from "../AnswerIcon";
import WRMButton from "../styles/elements/WRMButton";
import WRMInput from "../styles/elements/WRMInput";
import { lg3Spacing, sm1Spacing } from "../styles/styleConfig";
import { lg1FontSize, lg2FontSize, WRMText } from "../styles/Typography";
import { QuestionDataType } from "../../types/Types";

interface TenFrameProps extends QuestionDataType {
  setNextQuestion: () => void;
  results: any;
  setResults: any;
}

const TenFrame = ({
  question,
  setNextQuestion,
  results,
  setResults,
}: TenFrameProps) => {
  const [input, setInput] = useState();
  const [displayIcon, setDisplayIcon] = useState(false);
  const correctAnswer = input === question.number;

  const onSubmitQuestion = () => {
    setDisplayIcon(true);
    if (correctAnswer) {
      setResults([
        ...results,
        { questionType: question.type, userInput: input, correct: true },
      ]);
    } else {
      setResults([
        ...results,
        { questionType: question.type, userInput: input, correct: false },
      ]);
    }
    setTimeout(() => {
      setNextQuestion();
    }, 500);
  };

  const tenFrame = (number: any) => {
    const cells = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    const dots = number;
    const cellData = [];
    for (let i = 1; i <= cells.length; i++) {
      if (i <= dots) {
        cellData.push(
          <FrameCell>
            <Circle />
          </FrameCell>
        );
      } else {
        cellData.push(<FrameCell />);
      }
    }
    return cellData;
  };

  return (
    <>
      <Header>
        <WRMText mt={lg3Spacing} fs={lg2FontSize}>
          {question.question}
        </WRMText>
        {displayIcon && <AnswerIcon correct={correctAnswer} />}
      </Header>

      <FrameContainer>
        <FrameRow>{tenFrame(question.number)}</FrameRow>
      </FrameContainer>
      <WRMInput input={input} setInput={setInput} />
      <WRMButton
        fs={lg1FontSize}
        color={Colors.fontWhite}
        text="Check"
        onPress={onSubmitQuestion}
        disabled={!input}
      />
    </>
  );
};

export default TenFrame;

const Header = styled.View`
  height: 100px;
  align-items: center;
  justify-content; center;
`;
const FrameContainer = styled.View`
  flex: 1;
  justify-content: center;
`;
const FrameRow = styled.View`
  flex-direction: row;
  justify-content: center;
  flex-wrap: wrap;
`;
const FrameCell = styled.View`
  align-items: center;
  justify-content: center;
  border-color: ${Colors.lightBlue};
  border-width: ${sm1Spacing}px;
  height: 60px;
  width: 60px;
`;
const Circle = styled.View`
  height: 40px;
  width: 40px;
  background-color: ${Colors.blue};
  border-radius: 50%;
`;
