import styled from "styled-components/native";
import Colors from "../../../constants/Colors";
import { lg3Spacing, md2Spacing } from "../styleConfig";

export const ScreenContainer = styled.View`
  flex: 1;
  padding: ${md2Spacing}px;
  padding-top: ${lg3Spacing}px;
  align-items: center;
  justify-content: center;
  background-color: ${Colors.fontWhite};
`;
