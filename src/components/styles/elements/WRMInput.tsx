import React, { useState } from "react";
import styled from "styled-components/native";
import Colors from "../../../constants/Colors";
import { lg1FontSize } from "../Typography";
import { md2Spacing, sm2Spacing } from "../styleConfig";

type InputProps = {
  input: any;
  setInput: any;
};

const WRMInput = ({ input, setInput }: InputProps) => (
  <StyledInput
    value={input}
    onChangeText={(value) => setInput(parseInt(value))}
    placeholder="Enter number"
    placeholderTextColor={Colors.fontWhite}
  />
);

export default WRMInput;

const StyledInput = styled.TextInput`
  background-color: ${Colors.grey};
  border-radius: ${sm2Spacing}px;
  font-size: ${lg1FontSize}px;
  align-self: stretch;
  justify-content: center;
  color: ${Colors.fontWhite};
  align-items: center;
  padding: ${md2Spacing}px;
`;
