import styled from "styled-components/native";
import Colors from "../../../constants/Colors";
import { WRMText } from "../Typography";
import { btnHeight, lg1Spacing, sm2Spacing } from "../styleConfig";

type StyleProps = {
  disabled?: boolean;
};
interface ProgressBarProps extends StyleProps {
  text: string;
  color?: string;
  fs?: number;
  onPress: any;
}

const WRMButton = ({
  fs,
  text,
  color,
  onPress,
  disabled,
}: ProgressBarProps) => (
  <StyledButton onPress={onPress} disabled={disabled}>
    <WRMText fs={fs} color={color}>
      {text}
    </WRMText>
  </StyledButton>
);

export default WRMButton;

const StyledButton = styled.Pressable<StyleProps>`
  background-color: ${({ disabled }) =>
    disabled ? Colors.lightGrey : Colors.successLight}
  height: ${btnHeight}px;
  margin-vertical: ${lg1Spacing}px;

  align-self: stretch;
  justify-content: center;
  align-items: center;

  border-radius: ${sm2Spacing}px;
  border-color: ${({ disabled }) =>
    disabled ? Colors.lightGrey : Colors.successDark}
  border-bottom-width: ${sm2Spacing}px;
`;
