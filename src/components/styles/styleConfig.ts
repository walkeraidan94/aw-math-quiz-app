export const sm1Spacing = 2;
export const sm2Spacing = 4;
export const sm3Spacing = 6;
export const sm4Spacing = 8;
export const md1Spacing = 12;
export const md2Spacing = 16;
export const md3Spacing = 24;
export const lg1Spacing = 28;
export const lg2Spacing = 32;
export const lg3Spacing = 48;
export const xl1Spacing = 56;

export const btnHeight = 50;
