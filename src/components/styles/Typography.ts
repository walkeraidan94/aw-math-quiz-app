import styled from "styled-components/native";
import Colors from "../../constants/Colors";

export const sm1FontSize = 12;
export const md1FontSize = 14;
export const lg1FontSize = 22;
export const lg2FontSize = 26;
export const xl1FontSize = 34;

type TextStyleProps = {
  fs?: number;
  color?: string;
  mt?: number;
};

export const WRMText = styled.Text<TextStyleProps>`
  font-weight: bold;
  font-size: ${({ fs }) => fs || md1FontSize}px;
  color: ${({ color }) => color || Colors.fontMain};
  margin-top: ${({ mt }) => mt || 0}px;
`;
