import React from "react";
import styled from "styled-components/native";
// Wasnt sure how to fix ts error for this at the time
import CorrectIcon from "../../assets/images/done.png";
import IncorrectIcon from "../../assets/images/cross.png";

type StyleProps = {
  resultSize?: boolean;
};
interface IconProps extends StyleProps {
  correct: boolean;
}

const AnswerIcon = ({ correct, resultSize }: IconProps) => {
  return (
    <Icon
      source={correct ? CorrectIcon : IncorrectIcon}
      resultSize={resultSize}
    />
  );
};

export default AnswerIcon;

const Icon = styled.Image<StyleProps>`
  height: ${({ resultSize }) => (resultSize ? 40 : 100)}px;
  width: ${({ resultSize }) => (resultSize ? 40 : 100)}px;
`;
