import React from "react";
import styled from "styled-components/native";
import Colors from "../../constants/Colors";

type ProgressBarProps = {
  progress: number;
};

const ProgressBar = ({ progress }: ProgressBarProps) => {
  return (
    <Container>
      <Foreground progress={progress}>
        <Highlight />
      </Foreground>
    </Container>
  );
};

export default ProgressBar;

const Container = styled.View`
  background-color: lightgrey;
  height: 20px;
  flex: 1;
  border-radius: 15px;
`;
const Foreground = styled.View<ProgressBarProps>`
  flex: 1;
  border-radius: 15px;
  background-color: ${Colors.progressFg};
  width: ${({ progress }) => (progress ? `${progress * 100}%` : "0px")};
`;
const Highlight = styled.View`
  width: 90%;
  height: 5px;
  border-radius: 5px;
  align-self: center;
  background-color: ${Colors.progressHighlight};
`;
