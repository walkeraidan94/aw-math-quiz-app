import React from "react";
import { WRMText, xl1FontSize } from "../styles/Typography";
import Colors from "../../constants/Colors";
import styled from "styled-components/native";
import {
  md1Spacing,
  sm1Spacing,
  sm2Spacing,
  sm3Spacing,
  sm4Spacing,
} from "../styles/styleConfig";

type StyleProps = {
  isSelected?: boolean;
  fillInBlank?: boolean;
  grid?: boolean;
};

interface NumberOptionProps extends StyleProps {
  number?: any;
  onPress?: any;
}

const NumberOption = ({
  number,
  isSelected,
  onPress,
  fillInBlank,
  grid,
}: NumberOptionProps) => {
  return (
    <OptionContainer
      grid={grid}
      isSelected={isSelected}
      onPress={onPress}
      fillInBlank={fillInBlank}
    >
      <WRMText color={Colors.orange} fs={xl1FontSize}>
        {number}
      </WRMText>
    </OptionContainer>
  );
};

export default NumberOption;

const OptionContainer = styled.Pressable<StyleProps>`
  ${({ isSelected }) =>
    isSelected && `background-color: ${Colors.orangeLight}`};
  border-width: ${sm1Spacing}px;
  border-bottom-width: ${sm3Spacing}px;
  border-color: ${Colors.orange};
  border-radius: ${sm4Spacing}px;
  width: ${({ fillInBlank }) => (fillInBlank ? "60px" : "48%")};
  ${({ grid }) => grid && `height: 30%}`};
  ${({ fillInBlank }) => fillInBlank && `margin: ${md1Spacing}px`}
  padding: ${sm2Spacing}px;
  margin-top: ${md1Spacing}px;
  align-items: center;
  justify-content: center;
`;
