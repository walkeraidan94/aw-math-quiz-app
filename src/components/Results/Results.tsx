import React from "react";
import styled from "styled-components/native";
import Colors from "../../constants/Colors";
import {
  lg1Spacing,
  md1Spacing,
  sm2Spacing,
  sm4Spacing,
} from "../styles/styleConfig";
// Typescript error needs fixed
import MonkeyHead from "../../assets/images/monkeyhead.png";
import { md1FontSize, WRMText, xl1FontSize } from "../styles/Typography";
import { TEN_FRAME, NUMBER_MULTIPLE_CHOICE } from "../../constants/Variables";
import AnswerIcon from "../AnswerIcon";

type StyleProps = {
  hideBorderRight?: boolean;
};
interface ResultsProps extends StyleProps {
  results: any;
}

const Results = ({ results }: ResultsProps) => {
  const framerHeader = ["Question", "Your Answer", "Correct"];
  return (
    <Container>
      <MonkeyImg source={MonkeyHead} />
      <WRMText color={Colors.successDark} fs={xl1FontSize}>
        Great effort
      </WRMText>
      <WRMText color={Colors.successDark} fs={xl1FontSize}>
        You rock!
      </WRMText>
      <FrameContainer>
        <HeaderRow>
          {framerHeader.map((header) => (
            <HeaderCell hideBorderRight={header === "Correct"}>
              <WRMText color={Colors.fontWhite} fs={md1FontSize}>
                {header}
              </WRMText>
            </HeaderCell>
          ))}
        </HeaderRow>
        <QuestionTypeCol>
          {results.map((item) => {
            const { questionType, userInput, correct } = item;
            return (
              <Row>
                <AnswerCell>
                  <WRMText>
                    {questionType === TEN_FRAME
                      ? "Ten frame"
                      : questionType === NUMBER_MULTIPLE_CHOICE
                      ? "Multi choice"
                      : "Fill in blanks"}
                  </WRMText>
                </AnswerCell>
                <AnswerCell>
                  <WRMText>{userInput}</WRMText>
                </AnswerCell>
                <AnswerCell hideBorderRight>
                  <AnswerIcon correct={correct} resultSize />
                </AnswerCell>
              </Row>
            );
          })}
        </QuestionTypeCol>
      </FrameContainer>
    </Container>
  );
};

export default Results;

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  margin-top: ${lg1Spacing}px;
`;
const FrameContainer = styled.View`
  flex: 1;
  margin-top: ${md1Spacing}px;
`;
const Row = styled.View`
  flex-direction: row;
`;
const HeaderRow = styled.View`
  flex-direction: row;
  border-top-width: 1px;
  border-bottom-width: 1px;
  border-left-width: 1px;
  border-right-width: 1px;
  border-color: ${Colors.fontMain};
`;
const QuestionTypeCol = styled.View`
  border-left-width: 1px;
  border-right-width: 1px;
  border-color: ${Colors.fontMain};
`;
const HeaderCell = styled.View<StyleProps>`
  background-color: ${Colors.lightBlue};
  padding: ${sm4Spacing}px 0;
  width: 100px;
  align-items: center;
  justify-content: center;
  border-right-width: ${({ hideBorderRight }) => (hideBorderRight ? 0 : 1)}px;
  border-color: ${Colors.fontMain};
`;
const AnswerCell = styled.View<StyleProps>`
  align-items: center;
  justify-content: center;
  width: 100px;
  padding: ${sm4Spacing}px 0;
  background-color: ${Colors.lightGrey};
  border-right-width: ${({ hideBorderRight }) => (hideBorderRight ? 0 : 1)}px;
  border-bottom-width: 1px;
`;
const MonkeyImg = styled.Image`
  height: 100px;
  width: 100px;
  margin-bottom: ${sm2Spacing}px;
`;
