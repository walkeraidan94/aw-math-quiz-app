import React, { useState } from "react";
import styled from "styled-components/native";
import Colors from "../../constants/Colors";
import AnswerIcon from "../AnswerIcon";
import NumberOption from "../NumberOption";
import WRMButton from "../styles/elements/WRMButton";
import {
  lg3Spacing,
  md1Spacing,
  sm2Spacing,
  xl1Spacing,
} from "../styles/styleConfig";
import { lg1FontSize, lg2FontSize, WRMText } from "../styles/Typography";
import { QuestionDataType } from "../../types/Types";

interface FillInTheBlankProps extends QuestionDataType {
  setNextQuestion: () => void;
  results: any;
  setResults: any;
}

const FillInTheBlankSingle = ({
  question,
  setNextQuestion,
  results,
  setResults,
}: FillInTheBlankProps) => {
  const onButtonPress = () => {
    setNextQuestion();
    setSelectedNumber(null);
  };

  const [selectedNumber, setSelectedNumber] = useState(null);
  const [displayIcon, setDisplayIcon] = useState(false);
  const correctAnswer = selectedNumber === question.correct;

  const onSubmitQuestion = () => {
    setDisplayIcon(true);
    if (correctAnswer) {
      setResults([
        ...results,
        {
          questionType: question.type,
          userInput: selectedNumber,
          correct: true,
        },
      ]);
    } else {
      setResults([
        ...results,
        {
          questionType: question.type,
          userInput: selectedNumber,
          correct: false,
        },
      ]);
    }
    setTimeout(() => {
      setNextQuestion();
    }, 500);
  };

  return (
    <>
      <Header>
        <WRMText mt={lg3Spacing} fs={lg2FontSize}>
          {question.question}
        </WRMText>
        {displayIcon && <AnswerIcon correct={correctAnswer} />}
      </Header>

      <Row>
        <WRMText mt={lg3Spacing} fs={lg2FontSize}>
          {question.text}
        </WRMText>
        <Blank>
          {selectedNumber && (
            <NumberOption
              number={selectedNumber.toString()}
              onPress={() => setSelectedNumber(null)}
            />
          )}
        </Blank>
      </Row>
      <OptionsContainer>
        {question.options.map((option: any) => (
          <NumberOption
            fillInBlank
            key={option.id}
            number={option}
            isSelected={selectedNumber === option}
            onPress={() => setSelectedNumber(option)}
          />
        ))}
      </OptionsContainer>
      <WRMButton
        fs={lg1FontSize}
        color={Colors.fontWhite}
        text="Check"
        onPress={onSubmitQuestion}
        disabled={!selectedNumber}
      />
    </>
  );
};

export default FillInTheBlankSingle;

const Header = styled.View`
  height: 160px;
  align-items: center;
  justify-content; center;
`;
const Row = styled.View`
  flex-direction: row;
  align-self: flex-start;
  margin-vertical: ${xl1Spacing}px;
`;
const Blank = styled.View`
  border-bottom-width: ${sm2Spacing}px;
  border-color: ${Colors.grey};
  width: 110px;
  margin-left: ${md1Spacing}px;
  align-items: center;
`;
const OptionsContainer = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  align-content: center;
  flex-direction: row;
  flex-wrap: wrap;
`;
