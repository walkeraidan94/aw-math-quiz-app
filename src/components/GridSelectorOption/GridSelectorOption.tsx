import React, { useState } from "react";
import styled from "styled-components/native";
import Colors from "../../constants/Colors";
import NumberOption from "../NumberOption";
import WRMButton from "../styles/elements/WRMButton";
import AnswerIcon from "../AnswerIcon";
import { QuestionDataType } from "../../types/Types";
import { lg3Spacing } from "../styles/styleConfig";
import { lg1FontSize, lg2FontSize, WRMText } from "../styles/Typography";

interface GridSelectorOptionProps extends QuestionDataType {
  setNextQuestion: () => void;
  results: any;
  setResults: any;
}

const GridSelectorOption = ({
  question,
  setNextQuestion,
  results,
  setResults,
}: GridSelectorOptionProps) => {
  const [selectedNumber, setSelectedNumber] = useState<number | null>(null);
  const [displayIcon, setDisplayIcon] = useState(false);
  const correctAnswer = selectedNumber === question.correct[0];

  const onSubmitQuestion = () => {
    setDisplayIcon(true);
    if (correctAnswer) {
      setResults([
        ...results,
        {
          questionType: question.type,
          userInput: selectedNumber,
          correct: true,
        },
      ]);
    } else {
      setResults([
        ...results,
        {
          questionType: question.type,
          userInput: selectedNumber,
          correct: false,
        },
      ]);
    }
    setTimeout(() => {
      setNextQuestion();
    }, 500);
  };

  return (
    <>
      <Header>
        <WRMText mt={lg3Spacing} fs={lg2FontSize}>
          {question.question}
        </WRMText>
        {displayIcon && <AnswerIcon correct={correctAnswer} />}
      </Header>

      <OptionsContainer>
        {question.options.map((option: any) => (
          <NumberOption
            grid
            number={option}
            isSelected={selectedNumber === option}
            onPress={() => setSelectedNumber(option)}
          />
        ))}
      </OptionsContainer>

      <WRMButton
        fs={lg1FontSize}
        color={Colors.fontWhite}
        text="Check"
        onPress={onSubmitQuestion}
        disabled={!selectedNumber}
      />
    </>
  );
};

export default GridSelectorOption;

const Header = styled.View`
  height: 100px;
  align-items: center;
  justify-content; center;
`;
const OptionsContainer = styled.View`
  flex: 1;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-around;
  align-content: center;
`;
