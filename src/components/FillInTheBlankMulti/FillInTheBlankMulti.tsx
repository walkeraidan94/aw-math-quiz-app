import React, { useState } from "react";
import styled from "styled-components/native";
import Colors from "../../constants/Colors";
import AnswerIcon from "../AnswerIcon";
import NumberOption from "../NumberOption";
import WRMButton from "../styles/elements/WRMButton";
import { lg3Spacing, md1Spacing, sm2Spacing } from "../styles/styleConfig";
import { lg1FontSize, lg2FontSize, WRMText } from "../styles/Typography";
import { QuestionDataType } from "../../types/Types";

interface FillInTheBlankProps extends QuestionDataType {
  setNextQuestion: () => void;
  results: any;
  setResults: any;
}

const FillInTheBlankMulti = ({
  question,
  setNextQuestion,
}: FillInTheBlankProps) => {
  const onButtonPress = () => {
    setDisplayIcon(true);
    setTimeout(() => {
      setNextQuestion();
    }, 500);
  };

  const [parts, setParts] = useState(question.parts);
  const [displayIcon, setDisplayIcon] = useState(false);

  const checkIfCorrect = () => {
    return (
      parts.filter((part) => part.isBlank && part.selected !== part.number)
        .length === 0
    );
  };

  const correct = checkIfCorrect();

  const addOptionToSelected = (option) => {
    if (isSelected(option)) {
      return;
    }

    const newParts = [...parts];
    for (let i = 0; i < newParts.length; i++) {
      if (newParts[i].isBlank && !newParts[i].selected) {
        newParts[i].selected = option;
        break;
      }
    }
    setParts(newParts);
  };

  const removeSelectedAt = (index) => {
    const newParts = [...parts];
    newParts[index].selected = null;
    setParts(newParts);
  };

  const isSelected = (option: number) => {
    return (
      parts.filter((part) => part.isBlank && part.selected === option).length >
      0
    );
  };

  const isReadyToCheck = () => {
    return parts.filter((part) => part.isBlank && !part.selected).length > 0;
  };

  return (
    <>
      <Header>
        <WRMText mt={lg3Spacing} fs={lg2FontSize}>
          {question.question}
        </WRMText>
        {displayIcon && <AnswerIcon correct={correct} />}
      </Header>

      <Row>
        {parts.map((part, index) => {
          // render out the 2 selected words
          if (part.isBlank) {
            return (
              <Blank>
                {part.selected && (
                  <NumberOption
                    number={part.selected}
                    onPress={() => removeSelectedAt(index)}
                  />
                )}
              </Blank>
            );
          } else {
            return (
              <WRMText mt={lg3Spacing} fs={lg2FontSize}>
                {part.number}
              </WRMText>
            );
          }
        })}
      </Row>

      <OptionsContainer>
        {question.options.map((option: any) => (
          <NumberOption
            fillInBlank
            key={option.id}
            number={option}
            isSelected={isSelected(option)}
            onPress={() => addOptionToSelected(option)}
          />
        ))}
      </OptionsContainer>
      <WRMButton
        fs={lg1FontSize}
        color={Colors.fontWhite}
        text="Check"
        onPress={onButtonPress}
        disabled={isReadyToCheck()}
      />
    </>
  );
};

export default FillInTheBlankMulti;

const Header = styled.View`
  height: 150px;
  align-items: center;
  justify-content; center;
`;
const Row = styled.View`
  flex-direction: row;
  align-self: flex-start;
  margin-top: 90px;
`;
const Blank = styled.View`
  border-bottom-width: ${sm2Spacing}px;
  border-color: ${Colors.grey};
  width: 110px;
  margin-left: ${md1Spacing}px;
  align-items: center;
`;
const OptionsContainer = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  align-content: center;
  flex-direction: row;
  flex-wrap: wrap;
`;
