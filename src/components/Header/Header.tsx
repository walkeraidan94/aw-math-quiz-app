import React from "react";
import styled from "styled-components/native";
import ProgressBar from "../ProgressBar";
import { lg1Spacing } from "../styles/styleConfig";

type HeaderProps = {
  progress: number;
};

const Header = ({ progress }: HeaderProps) => {
  return (
    <Container>
      <ProgressBar progress={progress} />
    </Container>
  );
};

export default Header;

const Container = styled.View`
  margin-top: ${lg1Spacing}px;
  flex-direction: row;
  align-items: center;
`;
