const fontMain = "#000";
const fontWhite = "#FFF";
const progressFg = "#FAC800";
const progressHighlight = "#FAD131";
const successLight = "#58CC02";
const successDark = "#57A600";
const orange = "#ED7014";
const orangeLight = "#f8cbaa";
const lightGrey = "#ebebeb";
const grey = "#a4a6a8";
const blue = "#0080FE";
const lightBlue = "#70b7ff";

export default {
  fontMain,
  fontWhite,
  progressFg,
  progressHighlight,
  successLight,
  successDark,
  orange,
  orangeLight,
  lightGrey,
  grey,
  blue,
  lightBlue,
  red,
  lightRed,
  bgMain,
};
