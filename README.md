# White Rose Maths Tech Test - Aidan Walker
**To build an npm/yarn install and npm start should be all thats required.**
## Overview
<p>From looking at some of the quiz challenges on the WRM app, I wanted to combine a few of the questions together aswell as implement the ten frame. I thought it could be a potential nice feature on the app to have a new quiz section for testing general knowledge from all the previous topics - Subitising, Addition, Subtration etc in one quiz for advanced study.</p>

## How it works
<p>I've created 4 different quiz types:</p>

- Ten frame input choice - Input the displayed amount of dots the ten frame shows.
    - Generates random number from 0 - 10 and compares answer with the input value.
- Multi choice (multiple of) number selection - Select 1 out of 4 numbers.
    - Checks for a multiple number provided in the data against the provided array.
- Single fill in the blank - Select 1 out of 4 numbers to fill in the last value in the sequence.
    - Checks the selected number is correct against the hard coded correct number provided in the data.
- Multi (2) numbers fill in the blank sequence.
    - Checks the 2 selected numbers match against the 2 hard coded number objects in the data(in the correct order) to correctly answer the missing sequences.

# Additional components
- Progress bar to indicate how far through the quiz you are.
- Answer icon to display a correct or incorrect image after submitting each question.

# Issues I ran into
- Its been a while since I used typescript so theres quite a few type errors and I specified a couple of types as any which I know defeats the purpose of ts. I left some dedicated time to fix some but I didnt want to spend too long struggling on them. (I will start brushing up on my ts again :). I also had some issues with unique keys which I needed to spend some more time looking at. I didnt want to leave it too long to return the test, apologies for these.

# Time Spent - Estimate would be around 10-12 hours

# Improvements
- I ran out of time on a few things which I would have liked to provide more dynamically:
    - For the multi choice question, instead of a hard coded array, I would have generated a random array with 4 numbers between maybe 1 - 30/40 etc and check that one of the numbers generated will be a multiple of the provided number.
- I ran out of time to display the actual question answer in the results.
- Some of the px values are hard coded and wont look the same on different devices (I did a lot of the styles quite fast) so there are a lot of improvements that could be made there.
- I would have added a start and restart button at the end and also provided local storage to save the current question the user is on. 
- A start button for an advanced mode, which would record user lives(for example 5) and deduct a life for each incorrect answer.
- An algorithm would have been nice for the fill in the blanks instead of hard coded answers.
